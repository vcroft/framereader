////////////// header_preprocess.h ////////////////
// Exists solely for the purpose of handling the //
// jet cuts - perhaps it doesn't have to be done //
// this way, but this works well.                //
///////////////////////////////////////////////////

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include <Math/GenVector/LorentzVector.h>
#include <Math/GenVector/PtEtaPhiM4D.h>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <iostream>
//#include <format>
#include <sstream>
 
using namespace ROOT::VecOps;
using std::string;

using rvec_f = const RVec<float> &;
using rvec_i = const RVec<float> &;
using rvec_vf = const RVec<RVec<float>> &;
using rvec_b = const RVec<bool> &;
using rvec_vb = const RVec<RVec<bool>> &;
using rvec_s = const RVec<string> &;
using rvec_vs = const RVec<RVec<string>> &;

// Gets jvtSF for each jet in an event
float extend_JvtSF(size_t njet, rvec_vf jet_JvtEff_SF_Tight, rvec_b is_good_jet)
{
    float jvtSF = 1.0;
    for (size_t i = 0; i < njet; i++){
        if (is_good_jet[i]){
            jvtSF = jvtSF*jet_JvtEff_SF_Tight[i][0];
        }
    }
    return jvtSF;
}

//Using these functions, and then selecting events with n_good_lepton == 2, is the same as the individual cuts one at a time (Alec compared and proved this)
RVec<bool> isGoodLepton(size_t nlep, rvec_f lep_pt, float ptCut, rvec_f lep_eta, float etaCut, rvec_b lep_isTrigMatched)
{
    RVec<bool> is_good_lepton(nlep);
    for (size_t i = 0; i < nlep; i++){
        is_good_lepton[i] = (lep_pt[i] > ptCut && abs(lep_eta[i]) < etaCut);
        if (is_good_lepton[i] == false) {
            std::cout << "pT: " << lep_pt[i] << "; eta: " << lep_eta[i] << "; isTrigMatched: " << lep_isTrigMatched[i] << std::endl;
            std::cout << is_good_lepton[i] << std::endl;
        }
    }
    return is_good_lepton;
}

int nGoodLep(RVec<bool> is_good_lepton){
    int good = 0;
    for (size_t i = 0; i < is_good_lepton.size(); i++){
        if (is_good_lepton[i] == true){
            good++;
        }
    }
    return good;   
}

// Function to do trigger selection. Was originally done directly in preprocess (python), moved here so that the trigger list is available to be passed to the below trigger matching function
std::vector<string> getTriggerList(string year, string prod, bool isData, string channel, int rand_run_nr = 0){
    std::map< string, std::vector<string> > const ElectronTriggers {
        {"2015", {"HLT_e24_lhmedium_L1EM20VH","HLT_e60_lhmedium","HLT_e120_lhloose"} },
        {"2016", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} },
        {"2017", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} },
        {"2018", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} }
    };
    std::map< string, std::vector<string> > MuonTriggers {
        {"2015", {"HLT_mu20_iloose_L1MU15","HLT_mu50"} },
        {"2016", {"HLT_mu26_ivarmedium","HLT_mu50"} },
        {"2017", {"HLT_mu26_ivarmedium","HLT_mu50"} },
        {"2018", {"HLT_mu26_ivarmedium","HLT_mu50"} }
    };
    std::map< string, std::map< string, std::vector<string> > > Triggers {
        {"el", ElectronTriggers },
        {"muon", MuonTriggers }
    };
    std::vector<string> Years;
    Years.push_back(year);
    if (!isData && prod == "a") {
        if (rand_run_nr < 297730) { // actually a 2015 event!
            Years.pop_back();
            Years.push_back("2015");
        }
    }
    std::vector<string> triggerList;
    for (string y : Years) {
        for (string trig: Triggers[channel][y]) {
            triggerList.emplace_back(trig);
        }
    }
    //for (auto i = triggerList.begin(); i != triggerList.end(); ++i)
        //std::cout << *i << " ";
    return triggerList;
}

// Get triggerSelection boolean
bool getTriggerSelection(string year, string prod, bool isData, string channel, RVec<string> passedTriggers, int rand_run_nr = 0){
    std::map< string, std::vector<string> > const ElectronTriggers {
        {"2015", {"HLT_e24_lhmedium_L1EM20VH","HLT_e60_lhmedium","HLT_e120_lhloose"} },
        {"2016", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} },
        {"2017", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} },
        {"2018", {"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"} }
    };
    std::map< string, std::vector<string> > MuonTriggers {
        {"2015", {"HLT_mu20_iloose_L1MU15","HLT_mu50"} },
        {"2016", {"HLT_mu26_ivarmedium","HLT_mu50"} },
        {"2017", {"HLT_mu26_ivarmedium","HLT_mu50"} },
        {"2018", {"HLT_mu26_ivarmedium","HLT_mu50"} }
    };
    std::map< string, std::map< string, std::vector<string> > > Triggers {
        {"el", ElectronTriggers },
        {"muon", MuonTriggers }
    };
    std::map< string, string > RunConditions;
    RunConditions.insert(std::pair<string,string>("2015"," < 297730"));
    RunConditions.insert(std::pair<string,string>("2016"," >= 297730"));
    std::vector<string> Years;
    //Years.push_back(year);
    if (!isData && prod == "a") { // Have to look at random run number for MC16a
        if (rand_run_nr < 297730) { // 2015 event
            //std::cout << "***2015 event" << std::endl;
            Years.push_back("2015");
        }
        else if (rand_run_nr >= 297730) { // 2016 event
            //std::cout << "**2016 event" << std::endl;
            Years.push_back("2016");
        }
    }
    else {
        Years.push_back(year);
    }
    bool trigSelection = false;
    for (string y : Years) {
        for (string trig: Triggers[channel][y]) {
            if (find(begin(passedTriggers), end(passedTriggers), trig) != end(passedTriggers)){
                //std::cout << "Passed trigger " << trig << "; good to go!" << std::endl;
                trigSelection = true;
                break;
            }
            //else {
                //std::cout << "Didn't pass trigger " << trig << std::endl;
            //}
        }
    }
    //std::cout << "***Trigger Selection result: " << trigSelection << std::endl;
    return trigSelection;
}

// Take triggerList from above function and does trigger matching
std::vector<bool> lepIsTrigMatched(string chan, size_t nlep, rvec_f lep_pt, rvec_vs lep_listTrigChains, RVec<RVec<int>> lep_isTrigMatchedToChain, rvec_s triggerList, rvec_s passedTriggers)
{
    std::vector<bool> is_trig_matched(nlep);
    for (size_t l = 0; l < nlep; l++){ // Looping over leptons
        //std::cout << "*****LEPTON #" << l << "*****" << std::endl;
        //std::cout << "***Trigger list: " << std::endl;
        //for (int i = 0; i < triggerList.size(); i++)
            //std::cout << triggerList[i] << std::endl;
        RVec<int> matches = lep_isTrigMatchedToChain[l];
        rvec_s trigChain = lep_listTrigChains[l];
        float pt = lep_pt[l];
        RVec<float> thresholds;
        for (size_t i = 0; i < trigChain.size(); i++){ // Looping over triggers
            string trig = trigChain[i];
            //std::cout << "**For trigger " << trig << std::endl; 
            if (std::find(triggerList.begin(), triggerList.end(), trig) == triggerList.end()){ // skip triggers not required
                //std::cout << "  This one isn't required." << std::endl;
                continue;
            }
            // First, see if lep is trigger matched
            bool matched = false;
            if (trig != "HLT_e300_etcut") {
                //std::cout << "  Matches? " << matches[i] << std::endl;
                if (matches[i] == 1) { matched = true; }
            }
            else { matched = true; }
            // Next, double-check that, if the lep is matched, it has the necessary pT for the matched trigger (so we don't have to deal with trig inefficiencies)
            if (matched && std::find(passedTriggers.begin(), passedTriggers.end(), trig) != passedTriggers.end()) { // muon matched to fired trigger
                string trigSub = trig.substr(trig.find("mu")+2,2);
                //std::cout << "  Threshold is: " << trigSub << std::endl;
                float threshold;
                if (chan == "muon") { threshold = std::stoi(trigSub)*1.05; }
                else { threshold = std::stoi(trigSub)+1; }
                thresholds.push_back(threshold);
            }
        }
        if (thresholds.size() == 0){ // lepton didn't match any trigger
            is_trig_matched[l] = false;
            //std::cout << "~~NOT trigger matched! No triggers matched~~" << std::endl;
            goto NEXTLEP; 
        }
        //std::cout << "Lepton pT: " << pt << std::endl;
        for (size_t t = 0; t < thresholds.size(); t++) {
            if (pt > thresholds[t]){
                is_trig_matched[l] = true;
                //std::cout << "~~Trigger matched! pT > " << thresholds[t] << std::endl;
                goto NEXTLEP;
            }
        }
        //std::cout << "~~NOT trigger matched! pT too low for all matched triggers" << std::endl;
        is_trig_matched[l] = false;
        NEXTLEP:
        //std::cout << "going to next lep..." << std::endl;
        int alec = 0;
    }
    return is_trig_matched;  
}

// Decides if jets in event are "good" jets. "Good" jets pass pT, eta, and JvtPass wp cut (for reco), and also must pass a dR cut with every lepton in the event
RVec<bool> isGoodJet(size_t njet, size_t nlep, rvec_f jet_pt, rvec_f jet_eta, rvec_f jet_phi, rvec_f jet_E, rvec_f lep_eta, rvec_f lep_phi, float pTCut, float etaCut, float dRCut, RVec<int> jet_JvtPass_Tight, bool isReco)
{
    RVec<bool> is_good_jet(njet);
    //std::cout << "*****Begin event*****" << std::endl;
    for (size_t i = 0; i < njet; i++){
        //std::cout << "***JET #" << i << std::endl;
        ROOT::Math::PtEtaPhiEVector jetTLV (jet_pt[i],jet_eta[i],jet_phi[i],jet_E[i]);
        if (isReco && !(jet_pt[i] > pTCut && std::abs(jetTLV.Rapidity()) < etaCut && jet_JvtPass_Tight[i] == 1)) { // Reco jets: must pass pT & eta cuts as well as Jvt
            is_good_jet[i] = false;
            continue;
            //std::cout << "--->Failed other cuts" << std::endl;
        }
        else if (!isReco && !(jet_pt[i] > pTCut && std::abs(jetTLV.Rapidity()) < etaCut)) { // Truth jets: just have to pass pT and eta cuts
            is_good_jet[i] = false;
            continue;
            //std::cout << "--->Failed other cuts" << std::endl;
        } 
        bool deltaR = true;
        float jphi = jet_phi[i];
        //if (jphi < 0) jphi += 2*M_PI;
        for (size_t j = 0; j < nlep; j++){
            float lphi = lep_phi[j];
            //if (lphi < 0) lphi += 2*M_PI;
            if (DeltaR(jet_eta[i], lep_eta[j], jphi, lphi) < dRCut){
                deltaR = false;
                //std::cout << "--->Failed deltaR for at least one lepton" << std::endl;
                //std::cout << "--->jet eta: " << jet_eta[i] << " lep eta: " << lep_eta[j] << " jet phi: " << jphi << " lep phi: " << lphi << std::endl;
            }
        }
        is_good_jet[i] = deltaR;
    }
    //std::cout << "is_good_jet results: " << is_good_jet << std::endl;
    //std::cout << "*****Done with this event*****" << std::endl;
    return is_good_jet;
}

// Sums the number of "good" jets in the event
int nGoodJet(RVec<bool> is_good_jet){
    int good = 0;
    for (size_t i = 0; i < is_good_jet.size(); i++){
        if (is_good_jet[i] == true) { good++; }
    }
    //std::cout << "          Sum: " << good << std::endl;
    return good;   
    //std::cout << "              Sum: " << ROOT::VecOps::Sum(is_good_jet) << std::endl; WHY ISN'T THIS WORKING?
    //return ROOT::VecOps::Sum(is_good_jet);
}

