# DataFrame Reader - Plan and Execution

## Quickstart
On the Tufts cluster, log into the RH7 node, then load ROOT 6.22 and RooUnfold:
```
srun -N1 -n2 -p rh7batch --time=2-2:00:00 --pty --x11=first bash
source /cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/ROOT/v6.22.00/x86_64-centos7-gcc8-opt/ROOT-env.sh
source /cluster/tufts/beaucheminlab/RooUnfold/build/setup.sh
```

then run!

```
./run_analysis
```

![a plot!](outputs/leadAllJet_pT.pdf)

## Step 1: Preprocessing
Inputs: ntuples from maker (e.g. https://gitlab.cern.ch/jbossios/MakeTTrees_ZHF/tree/master)
Outputs: A file for each DSID (or period) of the given MC production (or data year) containing branches a) carried over from the inputs and b) derived from the input branches (e.g. "is_good_jet")
Options:
- `-i` input configuration file
- `-o` output directory
- `-s` specific DSID or period (e.g. 364100) - produces file for just that identifier. Primarily to use with debugging
- `--debug` run over 100 events, with more print statements
To run:
```
./preprocess -i preprocessing/[input_config] -o [path/to/output/dir] [-s DSID --debug]
```

### Configuration files: 
Configuration files for the preprocessing step are located in the preprocessing/ directory. There are three up-to-date configuration files: v4\_{mc16a,data15,data16}\_stack.json. The configuration files specify the following:
- `isData` - flag to tell preprocess script if files are data or not
- `datasets` - For each DSID or period: the year, channel, production (a, d, or e), path to the files, and the AMI, sumWeights, and lumi values
- `systematics` - list of systematics; dataframe will be made for each one
- `variables` - definitions which you want added to the files, in format "name": "definition". Definitions are strings of C++ logic, to be read in by ROOT RDataFrame Define() command

### What does preprocess do?
- Loads the files from the config and constructs a dataframe for each systematic
- Extends the dataframe with the definitions of... 
1. ...the `variables` in the config
2. ...the DSID values (AMI, sumWeights, lumi)
3. ...variables related to jets (e.g. is_good_jet)
4. ...the scale factors
5. ...the trigger selection (done through functions in header_preprocess.py)
6. ...the trigger matching (also done in header_preprocess.py)
7. ...the event weights, including the overall histogram scaling values
- Creates a ROOT RSnapshot file, containing a) the above new definitions and b) any definitions we want to port over directly from the input files

### To do:
- [ ] un-hardcode channel, year, prod, isData at top of preprocess
- [ ] un-hardcode pT, eta cuts for isGoodJet header function


## Step 2: The main analysis code - run_analysis
Inputs: Preprocessed files from Step 1
Outputs:
- Plots of observables
- Unfolded distributions
- Cutflows
- Event printouts

### Configuration files:
Configuration files for run_analysis are located in the configurations/ directory; ones that need fixing should be stored in the fix/ subdirectory. The configuration files are meant to be used for a given analysis, i.e. a given combination of data+MC, in a given channel, etc. The configuration files specify the following:
- `processes` - processes correspond to the data types: data, mc signal, background. For each `process`, a `Frame` object will be created
--- For each process, is_mc_signal and is_data flags tell the code what the files contain, and the files, which are located at the file_prefix, are listed
- `systematics` - list of systematics; dataframe will be made for each one, for each process
- `common_cuts` - the selections, in format "name": "selection". Selections are strings of C++ logic, to be read in by ROOT RDataFrame Define() command
- `regions` - Analysis phase-space regions of interest, e.g. "e1jet." For each `region`, a `View` object will be created. A region consists of `cuts` to define the region and `observables` (histograms) to plot. The observables are defined using C++ defintions; binning and title info are also provided.

### What does run_analysis do?
1. Creates a analysis object using the specified config
2. A number of different commands can be specified within run_analysis: 
- `histogram_process('[process]')` produces histograms for a given process, for all regions
- `histogram_stack()` will produced histograms of all the processes in a stack for all regions
---`histogram_stack('[observable]','[region]')` will only plot a specific observable in a specific region
- `output_process('[process]')` creates a root file containing histograms for a given process
- `print_cutflow('[process]','[region]')` will print the number of events before and after each of the `common_cuts` for the given process in the given region
- `print_event('[[array of event numbers]]','[region]','[path/to/file]','[systematic]')` prints variable values for given events, in given region, for a given systematic, from a given file
- `unfold_process('[process]','[region]','observable]','[alg (default = svd)]','[reg (default = 5)]') will unfold observable of given process + region using given algorithm + regularization strength

### To do.
- [ ] figure out problem with output_process()

## Analysis code
The code which runs the analysis is in the analysiscode/ directory. Here is a summary of what the various scripts do.

### Analysis.py (analysis class)
`run_analysis` first calls Analysis.py to create an `analysis` object. For this object,
- Frame.py is called to create a `frame` object for each `process` (e.g. data, zjets)
- View.py is called to create a `view` object for each `region` (e.g. e1jet).
The various commands listed above are then called on the frame and view objects.

### Frame.py (frame class)
This class functions as a wrapper for the ROOT DataFrames in order to associate cuts, histograms, pdf etc to a group of systematics read from an input file.
- `data_loader` - reads nominal tree from root file
- `print_vars` - prints nominal variables in the nominal tree in a file
- `apply_cuts` - for year and channel

### View.py (view class)
This class packages the analysis regions together. For each region it gets the dataframes associated with the processes involved and extends them with the observable definitions, applies the cuts, and calls plotting and/or unfolding functions (in Plotter.py and Fold.py, respectively) as necessary.
- `extend` adds the observales to the dataframes
- `apply_cuts` applies the selections in `common_cuts` from the configuration file
- `create_histograms` ________
- `evaluate_hists` ________
- `create_folds` calls Fold.py to create fold objects
- `print_cutflow` uses the RDataFrame Report() functionality to print the number of events passing each selection
- `integral_all` gets histogram integrals
- `plot_all` is called by `histogram_process()` to create histograms for a given process. It calls single_hist() in Plotter.py
- `stack_all` is called by `histogram_stack()` to produce stack plots with all processes. It calls stack_hist() in Plotter.py
- `unfold_all` ________
______
______
- `output_tree` is called by `output_process()` and creates an RSnapshot ROOT file of a process
- `print_event` is called by `print_event()` in run_analysis and prints the values listed within for each event requested.

### Plotter.py (plots histograms)
- `plot_1d` - plots single nominal histogram, with optional systematic envelope.
- `plot_all` - plots all variables defined in histogram definitions.
- `plot_systematics` - for all variables, for all systematics, retrieves intepolated PDF and plots with prefit errors

### To do
- [x] data/mc plots
- [x] systematic overlay plots
- [ ] 2d plots
- [ ] Unfolding plots

### Fold.py (handles unfolding)

### Functionality
Creates a Fold class that contains all unfolding information for a given variable.

### To do
- [x] create response matrix from definitons
- [x] plot control information from definitions
- [ ] create roounfoldspec to handle bias, variance and coverage
- [ ] plot regularisation checks
- [ ] include systematics, to extend variance estimate
