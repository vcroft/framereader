import ROOT
import json
from .Frame import frame
from .View import region
# Speeding things up with parallelization
ROOT.gROOT.SetBatch(True)
ROOT.EnableImplicitMT()

class analysis:
    def __init__(self, config):
        self.configuration = json.load(open(config))
        self.frames = {}
        self.views = {}
        self.systematics = []
        
        print("***** Initializing the following processes:")
        for process in self.configuration['processes']:
            print("\t"+process)
            self.frames[process] = frame(process, self.configuration)
            self.systematics = self.frames[process].systematics

        print("***** The region(s) of interest are:")
        for region_name in self.configuration['regions']:
            print("---"+region_name)
            self.views[region_name] = region(region_name,
                                        self.frames,
                                        self.configuration['regions'][region_name]['cuts'],
                                        self.configuration['regions'][region_name]['observables']                                              
            )


    def print_cutflow(self, sample, region, systematic='nominal'):
        print("***** Doing cutflow for "+sample+" process in "+region+" region:")
        assert sample in self.frames, "sample not known, try {}".format(list(self.frames.keys()))
        assert region in self.views, "view not defined, try {}".format(list(self.views.keys()))
        assert systematic in self.systematics, "systematic {}, not defined, try {}".format(systematic, self.systematics)
        self.views[region].print_cutflow(sample, systematic)


    def histogram_process(self, sample,
                          observable = None,
                          region=None,
                          systematic='nominal',
                          out='outputs',
                          fmt='pdf'):

        if region is None:
            for region in self.views:
                if observable is None:
                    self.views[region].plot_all(sample, out=out, fmt=fmt)
                else:
                    self.views[region].plot_1d(sample, observable, out=out, fmt=fmt)
        else:
            if observable is None:
                self.views[region].plot_all(sample, out=out, fmt=fmt)
            else:
                self.views[region].plot_1d(sample, observable, out=out, fmt=fmt)

    def integral_process(self, sample,
                          observable = None,
                          region=None,
                          systematic='nominal',
                          out='outputs',
                          fmt='pdf'):

        if region is None:
            for region in self.views:
                if observable is None:
                    self.views[region].integral_all(sample, out=out, fmt=fmt)
                else:
                    self.views[region].print_integral(sample, out=out, fmt=fmt)
        else:
            if observable is None:
                self.views[region].integral_all(sample, out=out, fmt=fmt)
            else:
                self.views[region].print_integral(sample, observable, out=out, fmt=fmt)

    def histogram_stack(self, observable=None,
                        region=None,
                        systematic='nominal',
                        out='outputs',
                        fmt='pdf'):
        print("***** histogram_stack() called:")
        if region is None:
            print("---No region specified - will plot with all of them")
            for region in self.views:
                if observable is None:
                    self.views[region].stack_all(out=out, fmt=fmt)
                else:
                    self.views[region].plot_stack(observable, out=out, fmt=fmt)
        else:
            print("---Will plot just "+region+" region")
            if observable is None:
                self.views[region].stack_all(out=out, fmt=fmt)
            else:
                self.views[region].plot_stack(observable, out=out, fmt=fmt)


    def fit_process(self, sample, region=None):
        print("do something")


    def fit_stack(self, region=None):
        print("do something")


    def output_process(self, region):
        print("***** Executing output_process()...")
        print(self.views)
        self.views[region].output_tree()


    def print_event(self, event_number, region, filename, treename):
        self.views[region].print_event(event_number,filename, treename)


    def unfold_process(self,
                       sample,
                       region=None,
                       observable=None,
                       alg='svd', reg='5',
                       out='outputs', fmt='pdf'):

        #assert sample in self.frames, 'sample not known'

        if region is None:
            for region in self.views:
                if observable is None:
                    self.views[region].unfold_all(sample, alg, reg, out=out, fmt=fmt)
                else:
                    self.views[region].unfold_1d(sample, alg, reg, out=out, fmt=fmt)
        else:
            if observable is None:
                self.views[region].unfold_all(sample, alg, reg, out=out, fmt=fmt)
            else:
                self.views[region].unfold_1d(sample, observable, alg, reg, out=out, fmt=fmt)
                

    def flavour_fit(self, region=None):
        print("do something")
