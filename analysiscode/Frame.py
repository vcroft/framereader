import ROOT
import os, json
from array import array

class frame:
#    def __init__(self, inputs_file, systematics_file, prod, channel, doSFs, doSysts, p):
    def __init__(self, process_name, configuration, verbose=False):

        process = configuration['processes'][process_name]
        systematics = configuration['systematics']
        self.is_mc_signal = configuration['processes'][process_name]['is_mc_signal']
        self.is_data = configuration['processes'][process_name]['is_data']
        self.verbose=verbose
        self.df = {}
        self.systematics = []
        self.contributions = {}
        self.histograms = {}
        self.cut_reports = {}
        self.pdf = {}
        self.workspace = {}

        for contribution in process['contributions']:
            self.data_loader(contribution, process['contributions'][contribution], systematics)
            #self.extend(contribution,
                        #variable_dict = configuration['variables'],
                        #prod = process['contributions'][contribution]['prod'],
                        #channel = process['contributions'][contribution]['channel'])
            self.apply_cuts(contribution,
                            definitions=configuration['common_cuts'])
            
        if len(self.contributions) ==1:
            self.df = [self.contributions[c] for c in self.contributions][0]
        elif len(self.contributions) >1:
            self.merge_contributions()
        #self.print_vars()

    def data_loader(self, contribution, process, systematics):
        # Get C++ vector of files
        files = []
        contribution_dfs = {}
        skippedFiles = []

        for dataset in process["datasets"]:
            for line in process["datasets"][dataset]["files"]:
                files.append(process["file_prefix"]+line)

            cFiles = ROOT.std.vector('string')()
            for n in files:
                # Skip file if it doesn't have nominal tree (means dilepton cut removed all events, probably)
                fl = ROOT.TFile.Open(n)
                if not fl.Get("nominal"):
                    #print("Skipping file\n "+n+"\n -no nominal tree")
                    badFile = str(n.replace(process["file_prefix"],''))
                    skippedFiles.append(badFile) if badFile not in skippedFiles else skippedFiles
                    continue
                cFiles.push_back(n)

        # Get metadata --> sumWeights
        #metadataFileName = process["DSID"]+"_metadata.root"
        #metadataFile = ROOT.TFile.Open(metadataFileName)
        #metadataHist = metadataFile.Get("MetaData_EventCount")
        #sumW = str(metadataHist.GetBinContent(3))
        #print(sumW)
        # Get AMI file --> AMI values
        #AMIFile = open("ami_reader.txt",'r')
        #lines = AMIFile.readlines()
        #for line in lines:
            #Line = line.split()
            #if (str(Line[0]) == process["DSID"]):
                #amixss = str(Line[1])
                #amieff = str(Line[2])
                #if len(Line) > 3: amik = str(Line[3])
                #else: amik = "1.0"

            # Create a dataframe for each systematic, loading all files each time
            for systematic in systematics:
                if systematic not in self.systematics:
                    self.systematics.append(systematic)
                tree_name = "{}".format(systematic)
                contribution_dfs[systematic] = ROOT.RDataFrame(tree_name, cFiles) 

            self.contributions[contribution] = contribution_dfs
        if skippedFiles:
            print("Skipped the following "+contribution+" files - no nominal tree found:")
            print(skippedFiles)

    def print_vars(self):
        """ print all of the column names
        currently stored in the data frame """ 
        assert "nominal" in self.systematics
        d = self.df["nominal"].GetColumnNames()
#        names = [i for i in d.GetColumnNames()]
#        print("\n".join(names))
        print(d)


   # def extend(self, contribution,
   #            variable_dict={},
   #            prod = None,
   #            channel = None):
   #     """ Get a list of variable definitions from file 
   #     and apply them to all data frames """

   #     # get list of names and definitions
   #     definitions = variable_dict[channel]
   #     for name, definition in definitions.items():
   #         # loop over all systematic trees
   #         for syst in self.systematics:
   #             # create a dummy dataframe with new definitions
   #             df = self.contributions[contribution][syst].Define(name,definition)
   #             # assign the to be the default
   #             self.contributions[contribution][syst] = df 


    def apply_cuts(self, contribution,
                   definitions={}):

        for syst in self.systematics:
            dataframes = [self.contributions[contribution][syst]]
            for name, definition in definitions.items():
                if self.verbose:
                    print('applying cut', name)
                    print(definition)
                dataframes.append(dataframes[-1].Filter(definition, name))
            self.contributions[contribution][syst] = dataframes[-1] 


    def merge_contributions(self):
        for contribution in self.contributions:
            print("The plan:")
            print("First, we make a list of all our variables")
            print("Then, we snapshot each contribution only containing these variables")
            print("Finally, we set self.df to that for each process")
            # Alternatively if we care we can either merge histograms or
            # we can alter the maker to just have one set of variables inside.


    def cut_flow(self, systematic = 'nominal'):
        d = self.df[systematic].Report()
        d.Print()
