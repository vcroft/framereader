import ROOT
#ROOT.gSystem.Load("libRooUnfold")
from .Plotting import response_plotter, plot_unfolded, plot_unfolded_traditional, plot_unfolded_quick_traditional

class fold:
    def __init__(self, var, truth, reco, migration, definition):
        self.name = var
        self.truth = truth
        self.reco = reco
        self.hist_def = definition
        self.migration = migration
        response = ROOT.RooUnfoldResponse(reco, truth, migration)
        print('loopin')
        for i in range(0,response.Hresponse().GetNbinsX()+2):
            for j in range(0,response.Hresponse().GetNbinsY()+2):
                response.Hresponse().SetBinError(i,j,0)
        self.response = response
        self.unfolded_functions = {}
        self.unfolded_histograms = {}
        self.spec = None

        
    def unfold_traditional(self, name, alg, reg):
        print('Unfold "traditional":',alg,reg)
        unfold = alg(self.response, self.reco, reg)
        unfolded_hist = unfold.Hreco()
        self.unfolded_histograms[name] = unfolded_hist


    def parse_alg(self,alg,mode="trad"):
        if mode is "roofit":
            if "svd" in alg.lower():
                alg = ROOT.RooUnfolding.kSVD
            elif "bayes" in alg.lower():
                alg = ROOT.RooUnfolding.kBayes
            return alg
        elif mode is 'trad':
            if "bayes" in alg.lower():
                alg = ROOT.RooUnfoldBayes
            elif "svd" in alg.lower():
                alg = ROOT.RooUnfoldSvd
            else:
                print("alg not known",alg)
                alg = ROOT.RooUnfoldInvert
        return alg
            


    def unfold(self, alg_name, reg, data=None, name=None):
        if data is None:
            data = self.reco
            print("Unfolding closure")
        if name is None:
            name = alg_name+str(reg)
        alg = self.parse_alg(alg_name,'roofit')
        spec = ROOT.RooUnfoldSpec(name, name,
                                  self.truth, "obs_truth",
                                  self.reco, "obs_reco",
                                  self.response.Hresponse(), 0,
                                  data, True, 0.0005, False)
        func = spec.makeFunc(alg,reg)
        unfold = func.unfolding()
        print("saving unfolding function", name)
        self.unfolded_functions[name] = func
        self.spec = spec


    def plot_closure(self, alg_name, reg, out, fmt):
        name = alg_name+str(reg)
        if not name in self.unfolded_functions:
            self.unfold(alg_name, reg)
        plot_unfolded(self.unfolded_functions[name],
                      self.name+"_"+name, 
                      self.truth,
                      self.spec.makeHistogram(self.truth),
                      self.hist_def,
                      out, fmt)


    def plot_closure_traditional(self, alg_name, reg, out, fmt):
        name = alg_name+str(reg)
        alg = self.parse_alg(alg_name,'trad')
        if not name in self.unfolded_histograms:
            self.unfold_traditional(name, alg, reg)
        plot_unfolded_traditional(self.name+"_"+name, self.unfolded_histograms[name], self.truth, self.reco)
        #plot_unfolded(self.unfolded_functions[name],
                      #self.name+"_"+name, 
                      #self.truth,
                      #self.spec.makeHistogram(self.truth),
                      #self.hist_def,
                      #out, fmt)


    def plot_response(self,out,fmt):
        response_plotter(self.response,
                      self.name,
                      self.hist_def,
                      out,fmt)
