import ROOT
import array as arr
from prettytable import PrettyTable

from .Plotting import single_hist, stack_hist
from .Fold import fold
from .Colours import get_colours

class region:
    def __init__(self, name, frames, definition, observables):
        self.name = name
        self.systematics = []
        self.frames = {}
        self.data = []
        self.observables = {}
        self.histograms = {}
        self.hist_templates = {}
        self.folds = {}
        self.evaluated = False

        for process, frame in frames.items():
            if len(self.systematics) == 0:
                self.systematics = frame.systematics
            if frame.is_mc_signal:
                print("   "+"---> "+process+"is the mc_signal, so we split it into B, C, M, L")
                self.apply_cuts(process+"_B", frame, definition, "n_truth_b > 0 && n_truth_c == 0")
                self.apply_cuts(process+"_C", frame, definition, "n_truth_b == 0 && n_truth_c > 0")
                self.apply_cuts(process+"_M", frame, definition, "n_truth_b > 0 && n_truth_c > 0")
                self.apply_cuts(process+"_L", frame, definition, "n_truth_b == 0 && n_truth_c == 0")
            else:
                self.apply_cuts(process, frame, definition)
            if frame.is_data:
                self.data.append(process)
        self.extend(observables)
        self.create_histograms(observables)


    def extend(self, observable_dict={}):
        """ Get a list of variable definitions from file 
        and apply them to all data frames """
        # get list of names and definitions
        print("   "+"---> Extending the dataframes with the observables we want to plot (extend() in View.py)")
        for name, definition in observable_dict.items():
            self.observables[name] = definition
            # loop over all systematic trees
            for syst in self.systematics:
                for process in self.frames:
                    if process in self.data: # Change #1: extend truth only if not data
                        #print("is data",self.frames[process])
                        # create a dummy dataframe with new definitions
                        df_1 = self.frames[process][syst].Define('reco_'+name,definition['reco_definition'])
                        self.frames[process][syst] = df_1
                    else:
                        # create a dummy dataframe with new definitions
                        df_1 = self.frames[process][syst].Define('reco_'+name,definition['reco_definition'])
                        df_2 = df_1.Define('truth_'+name,definition['truth_definition'])
                        self.frames[process][syst] = df_2


    def apply_cuts(self, process, frame, definitions={}, signal_selection=None):
        print("   "+"---> "+"Making selections for "+process+" process (apply_cuts() in View.py)")
        self.frames[process] = {}
        for syst in self.systematics:
            if signal_selection is None:
                dataframes = [frame.df[syst]]
            else: # For splitting mc into B/C/M/L
                dataframes = [frame.df[syst].Filter(signal_selection)]
            for name, definition in definitions.items():
                dataframes.append(dataframes[-1].Filter(definition, name))
            self.frames[process][syst] = dataframes[-1]


    def create_histograms(self, observables):
        print("   "+"---> Creating histograms (create_histograms() in View.py)")
        for name, obs in  observables.items():
            # if the histogram models don't exist add them
            if name not in self.histograms:
                self.histograms[name] = {}
                self.hist_templates[name] = {}
                if not obs['vbinning']:
                    truth_hist_model = ROOT.RDF.TH1DModel('truth_'+name,'truth_'+name,
                                                         obs['truth_histogram'][0],
                                                         obs['truth_histogram'][1],
                                                         obs['truth_histogram'][2])
                    reco_hist_model = ROOT.RDF.TH1DModel('reco_'+name,'reco_'+name,
                                                         obs['reco_histogram'][0],
                                                         obs['reco_histogram'][1],
                                                         obs['reco_histogram'][2])
                    migration_model = ROOT.RDF.TH2DModel("migration_"+name,
                                                         "migration_"+name,
                                                         obs['reco_histogram'][0],
                                                         obs['reco_histogram'][1],
                                                         obs['reco_histogram'][2],
                                                         obs['truth_histogram'][0],
                                                         obs['truth_histogram'][1],
                                                         obs['truth_histogram'][2])
                else:
                    truth_hist_model = ROOT.RDF.TH1DModel('truth_'+name,'truth_'+name,
                                                          len(obs['truth_histogram'])-1,
                                                          arr.array('f',obs['truth_histogram']))
                    reco_hist_model = ROOT.RDF.TH1DModel('reco_'+name,'reco_'+name,
                                                         len(obs['reco_histogram'])-1,
                                                         arr.array('f',obs['reco_histogram']))
                    migration_model = ROOT.RDF.TH2DModel("migration_"+name,
                                                        "migration_"+name,
                                                        len(obs['reco_histogram'])-1,
                                                        arr.array('f',obs['reco_histogram']),
                                                        len(obs['truth_histogram'])-1,
                                                        arr.array('f',obs['truth_histogram']))
                self.histograms[name]['truth'] = truth_hist_model
                self.histograms[name]['reco'] = reco_hist_model
                self.histograms[name]['migration'] = migration_model
                self.histograms[name]['def'] = obs

            # add a dictionary of truth and reco histograms
            # for each systematic in the process.

            for process in self.frames:
                if process not in self.data:
                    self.hist_templates[name][process] = {'nominal' : {
                        'truth' : self.frames[process]['nominal'].Histo1D(
                            self.histograms[name]['truth'], 'truth_'+name,"histScale"),
                        'reco' : self.frames[process]['nominal'].Histo1D(
                            self.histograms[name]['reco'], 'reco_'+name,"histScale")}}

                    for systematic in self.systematics:
                        self.hist_templates[name][process][systematic] = {
                            'truth' : self.frames[process][systematic].Histo1D(
                                self.histograms[name]['truth'],
                                'truth_'+name,"histScale"),
                            'reco' : self.frames[process][systematic].Histo1D(
                                self.histograms[name]['reco'],
                                'reco_'+name,"histScale")
                        }
                        self.hist_templates[name][process]['migration'] = self.frames[process]['nominal'].Histo2D(self.histograms[name]['migration'],"reco_"+name,"truth_"+name)
                else:
                    self.hist_templates[name][process] = {'nominal' : {
                        'reco' : self.frames[process]['nominal'].Histo1D(
                            self.histograms[name]['reco'], 'reco_'+name,"histScale")}}
                    for systematic in self.systematics:
                        self.hist_templates[name][process][systematic] = {
                            'reco' : self.frames[process][systematic].Histo1D(
                                self.histograms[name]['reco'],
                                'reco_'+name,"histScale")
                        }
                    

    def evaluate_hists(self):
        for variable, definition in self.hist_templates.items():
            assert variable in self.histograms, 'variable not in histograms'
            for process, contents in definition.items():
                self.histograms[variable][process] = {}
                for systematic, variation in contents.items():
                    if systematic is 'migration':
                        self.histograms[variable][process][systematic] = self.hist_templates[variable][process][systematic].GetValue()
                    else:
                        if process in self.data: #Change #2 : Don't define truth if not data
                            self.histograms[variable][process][systematic] = {'reco':
                                    self.hist_templates[variable][process][systematic]['reco'].GetValue()}
                        else:
                            self.histograms[variable][process][systematic] = {'truth':
                                    self.hist_templates[variable][process][systematic]['truth'].GetValue(),
                                                                                'reco':
                                    self.hist_templates[variable][process][systematic]['reco'].GetValue()}
        self.evaluated = True


    def create_folds(self, process):
        """ Create unfolding object for each variable
        for which there is a Truth equivalent """
        
        assert "nominal" in self.systematics
        assert process in self.frames
        
        if self.evaluated is False:
            self.evaluate_hists()
            
        for var in self.histograms:
            if len(self.folds) == 0:
                self.folds[var] = {}
            elif var not in self.folds:
                self.folds[var] = {}
            elif process in self.folds[var]:
                print("already got this fold",process,var)
                return
            self.folds[var][process] = fold(var,
                                            self.histograms[var][process]['nominal']['truth'],
                                            self.histograms[var][process]['nominal']['reco'],
                                            self.histograms[var][process]['migration'],
                                            self.histograms[var]['def'])

    def print_cutflow(self, sample, variation="nominal"):
        assert sample in self.frames, "sample not in view"
        assert variation in self.systematics, "systematic not in view"
        d = self.frames[sample][variation].Report()
        d.Print()

    def integral_all(self, sample, out, fmt):
        for var in self.observables:
            self.print_integral(sample, var, out, fmt)

    def print_integral(self, sample, variable, out, fmt):
        
        if not self.evaluated:
            self.evaluate_hists()
        assert sample in self.histograms[variable], "sample {} not in view".format(sample)

        print("This will be the integral of " + str(variable))
        print(str(self.histograms[variable][sample]['nominal']['reco'].Integral()))

    def plot_all(self, sample, out, fmt):
        for var in self.observables:
            self.plot_1d(sample, var, out, fmt)

    def plot_1d(self, sample, variable, out, fmt):

        if not self.evaluated:
            self.evaluate_hists()
        assert sample in self.histograms[variable], "sample {} not in view".format(sample)
            
        single_hist(self.histograms[variable][sample]['nominal']['reco'],
                    sample+"_"+variable,
                    self.histograms[variable]['def'])

    def stack_all(self, out, fmt):
        for var in self.observables:
            self.plot_stack(var, out, fmt)

    def plot_stack(self, obs, out, fmt):
        #print("---Making plot stack (plot_stack() in View.py)")
        if not self.evaluated:
            self.evaluate_hists()
        mc_stack = ROOT.THStack()
        mc_hist_list = []
        bkg_hist_list = []
        haveData = False;
        for sample in self.frames: # Go through data, mc_signal, and mc_background frames ("sample")
            assert sample in self.histograms[obs]
            h = self.histograms[obs][sample]['nominal']['reco']
            h.Scale(1.0,"width")
            colour = get_colours(sample)
            if "data" in sample: # For the data frame....
                haveData = True
                h.SetLineColor(1)
                h.SetLineWidth(2)
                h.SetMarkerStyle(20)
                h.SetMarkerSize(0.65)
                h.SetMarkerColor(colour)
                dataPair = (sample,h) # ...pass a single hist as pair (name, hist)
            elif "background" in sample: # For the backgrounds frame...
                bkg_hist_list.append(h) # ...add it to bkg list
                #print("bkg sample: {}".format(sample))
                #print("bkg hist (pointer?): {}".format(h))
            else: # For mc_signal frame...
                h.SetFillColor(colour)
                h.SetLineColor(12)
                mc_hist_list.append((sample, h)) # ...add it to mc list

        mc_hist_list.sort(key = lambda x: x[1].Integral())
        for _,h in mc_hist_list:
            mc_stack.Add(h) # mc passed as stack
        
        if haveData:
            stack_hist(mc_stack, mc_hist_list, bkg_hist_list,
                    obs, self.histograms[obs]['def'], dataPair, out=out, fmt=fmt)
        else:
            stack_hist(mc_stack, mc_hist_list, bkg_hist_list,
                    obs, self.histograms[obs]['def'], out=out, fmt=fmt)
    
    def unfold_all(self, sample, alg, reg, out, fmt):
        for var in self.observables:
            self.unfold_1d(sample, var, alg, reg, out, fmt)


    def unfold_1d(self, sample, variable, alg, reg, out, fmt):
        if variable not in self.folds:
            print('*** Unfolding')
            self.create_folds(sample)
        elif sample not in self.folds[variable]:
            print("adding unfolded sample")
            self.create_folds(sample)
        self.plot_responses(self.folds[variable][sample])
        #self.plot_closure(self.folds[variable][sample], 'svd', 5, out, fmt)
        self.plot_closure_traditional(self.folds[variable][sample], alg, reg, out, fmt)

    def plot_responses(self, fold, out="outputs", fmt="pdf"):
        fold.plot_response(out,fmt)


    def plot_closure(self, fold, alg, reg, out="outputs", fmt="pdf"):
        fold.plot_closure(alg, reg, out, fmt)

    def plot_closure_traditional(self, fold, alg, reg, out="outputs", fmt="pdf"):
        fold.plot_closure_traditional(alg, reg, out, fmt)

    def output_tree(self, filename="outputs/snapshot.root"):
        opts = ROOT.RDF.RSnapshotOptions()
        opts.fMode = "UPDATE"
        opts.fOverwriteIfExists = True
        for sample in self.frames:
            branch_list = ROOT.vector('string')()
            for var in self.observables:
                print(var)
                branch_list.push_back("reco_"+var)
                branch_list.push_back("truth_"+var)
                
            snapshot_df = self.frames[sample]['nominal'].Snapshot(sample+'_tree', filename, branch_list, opts)


    # Read and print variable values of specific event(s) for event-by-event reader comparisons
    def print_event(self, event_number, filename, treename):
        # Load in preprocessed file
        f = ROOT.TFile.Open(filename)
        if isinstance(event_number,list):
            for events in event_number:
                for sample in self.frames:
                    #tree = f.Get(sample+"_tree")
                    tree = f.Get(treename)
                    for event in tree:
                        if event.mcEventNumber == events:
                            print("#################################################################################")
                            print("EVENTINFO: EventNumber: " + str(event.mcEventNumber))
                            print("EVENTINFO: njets: " + str(event.njet))
                            print("EVENTINFO: n(good)jets: " + str(event.n_good_jet))
                            print("EVENTINFO: good jets: " + str(event.is_good_jet))
                            #print("EVENTINFO: jet(0) pT: " + str(event.reco_leadAllJet_pT))
                            print("EVENTINFO: jet(0) pT: " + str(event.jet_pt[0]))
                            print("EVENTINF): jet(0) eta: " + str(event.jet_eta[0]))
                            print("EVENTINFO: jet(1) pT: " + str(event.jet_pt[1]))
                            print("EVENTINF): jet(1) eta: " + str(event.jet_eta[1]))
                            print("EVENTINFO: lep(0) pT: " + str(round(event.l0.Pt(),2)))
                            print("EVENTINFO: lep(0) eta: " + str(round(event.l0.Eta(),2)))
                            print("EVENTINFO: lep(1) pT: " + str(round(event.l1.Pt(),2)))
                            print("EVENTINFO: lep(1) eta: " + str(round(event.l1.Eta(),2)))
                            print("EVENTINFO: Z mass: " + str(round(event.zmass,2)))
                            print("EVENTINFO: Z pT: " + str(round(event.zpT,2)))
                            print("EVENTINFO: Z |y|: " + str(round(event.zabsy,2)))
                            print("EVENTINFO: Lepton SF: " + str(event.leptonSF))
                            print("EVENTINFO: JVT SF: " +str(event.jvtSF))
                            print("EVENTINFO: mcEventWeight: " + str(event.mcEventWeight))
                            print("EVENTINFO: pileup: " + str(event.weight_pileup))
                            print("EVENTINFO: weight: " + str(event.weight))
                            print("#################################################################################")
        print("Done!")

