import ROOT
import array as arr
import math

def plot_unfolded(unfolded, name, truth, spec_truth, hist_def,  out, fmt):
    # observable
    truth_obs_1 = unfolded.unfolding().response().Htruth().obs(0)
    h = truth.Clone()

    # set up canvas
    ROOT.gROOT.SetStyle("ATLAS")
    c = ROOT.TCanvas("c", "", 800, 1100)
    pad1 = ROOT.TPad("", "", 0., .45, 1., 1.)
    pad1.SetBottomMargin(0)
    pad1.Draw()
    pad11 = ROOT.TPad("", "ratio", 0., .3, 1., .45)
    pad11.SetTopMargin(0)
    pad11.SetBottomMargin(2)
    pad11.Draw()
    pad2 = ROOT.TPad("", "variance", 0.0, 0.2, 1., 0.3)
    pad2.SetTopMargin(2)
    pad2.SetBottomMargin(0)
    pad2.Draw()
    pad3 = ROOT.TPad("", "bias", 0.0, 0.1, 1.0, 0.2)
    pad3.SetTopMargin(0)
    pad3.SetBottomMargin(0)
    pad3.Draw()
    pad4 = ROOT.TPad("", "coverage", 0.0, 0.0, 1.0, 0.10)
    pad4.SetTopMargin(0)
    pad4.SetBottomMargin(2)
    pad4.Draw()
    if hist_def["logx"]:
        pad1.SetLogx()
    if hist_def["logy"]:
        pad1.SetLogy()
        h.GetYaxis().SetRangeUser(1,10*h.GetMaximum())
    else:
        h.GetYaxis().SetRangeUser(0,1.4*h.GetMaximum())
    pad1.cd()

    # Remove superfluous NPs from visualisation
    response_vars = []
    for param in unfolded.makeParameterList():
        if "response"in param.GetName():
            response_vars.append(param.GetName())
    unfolded_params = unfolded.makeParameterList()
    for param in response_vars:
        unfolded_params.remove(unfolded_params.find(param))
        
    # Plot
    h.Draw()
    frame = truth_obs_1.frame()
    unfolded.plotOn(frame,ROOT.RooFit.DrawOption("P"),
               ROOT.RooFit.Name(name),
               ROOT.RooFit.LineColor(29), ROOT.RooFit.LineWidth(3),
               ROOT.RooFit.VisualizeError(ROOT.RooFitResult.prefitResult(unfolded_params)))
    frame.Draw("same")



    xaxis = h.GetXaxis()
    xaxis.SetLabelSize(0.04)
    xaxis.SetTitleSize(0.045)
    xaxis.SetTitleOffset(.8)
    xaxis.SetTitle(hist_def["title"])
    # Add legend
    legend = ROOT.TLegend(0.65, 0.85, 0.92, 0.92)
    legend.SetTextFont(42)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetTextAlign(32)
    legend.AddEntry(h, "truth", "f")
    legend.AddEntry(frame.findObject(name),"unfolded "+name.split("_")[-1],"pl")

    legend.Draw("SAME")

    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.045)
    text.DrawLatex(0.21, 0.86, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.21 + 0.16, 0.86, "Preliminary")
    text.SetTextSize(0.04)
    text.DrawLatex(0.21, 0.80, "#sqrt{{s}} = 13 TeV, {:.1f} fb^{{-1}}".format(0.1))


    pad11.cd()
    unfolded_hist = frame.findObject(name).Clone()
    truth_histo = truth.Clone()

    for i in range(1,unfolded_hist.GetN()-1):
        truth_histo.SetBinContent(i,unfolded_hist.GetPointY(i))
        truth_histo.SetBinError(i,unfolded_hist.GetErrorY(i))
    target_sub = truth_histo
    target_sub.Divide(truth.Clone())

    target_sub.SetTitle("")
    target_sub.SetStats(0)
    y11 = target_sub.GetYaxis()
    y11.SetRangeUser(0.001,1.99)
    y11.SetLabelSize(.1)
    y11.SetTitle("Ratio")
    y11.SetTitleSize(.15)
    y11.SetTitleOffset(.25)
    y11.SetNdivisions(6)
    x11 = target_sub.GetXaxis()
    if hist_def['vbinning']:
        x11.SetRangeUser(hist_def['truth_histogram'][0],hist_def['truth_histogram'][-1])
    else:
        x11.SetRangeUser(hist_def['truth_histogram'][1],hist_def['truth_histogram'][-1])    
    x11.SetTitle("x")
    x11.SetTitleSize(.2)
    x11.SetTitleOffset(.5)
    x11.SetLabelSize(.1)

    target_sub.SetLineWidth(3)
    target_sub.SetLineColor(2)
    target_sub.Draw()

    l11 = ROOT.TLegend(0.7,0.7,0.9,0.9)
    l11.SetTextFont(42)
    l11.SetFillStyle(0)
    l11.SetBorderSize(0)
    l11.SetTextSize(0.1)
    l11.SetTextAlign(32)
    l11.AddEntry(target_sub,"Unfolded / Truth","l")
    l11.Draw()

    line11 = ROOT.TLine(hist_def["truth_histogram"][1], 1, hist_def["truth_histogram"][-1], 1)
    line11.SetLineColor(ROOT.kBlue)
    line11.SetLineWidth(1)
    line11.Draw("same")

    # Caluclate Bias and Coverage hists
    unfold_c = unfolded.unfolding()
    unfolded.unfolding().CalculateBias(ROOT.RooUnfolding.kBiasToys, 1000, spec_truth)
    biashist_cRMS = ROOT.RooUnfolding.convertTH1(unfold_c.Vbias(),unfold_c.Ebias(ROOT.RooUnfolding.kBiasRMS),unfold_c.response().Htruth())
    biashist_c = ROOT.RooUnfolding.convertTH1(unfold_c.Vbias(),unfold_c.Ebias(ROOT.RooUnfolding.kBiasSDM),unfold_c.response().Htruth())
    covhist_c = biashist_c.Clone("coverage")
    covVector_c = unfold_c.CoverageProbV()
    for i in range(covVector_c.GetNrows()):
        covhist_c.SetBinContent(i+1,covVector_c[i])
        covhist_c.SetBinError(i+1,0)
    varhist_c = biashist_c.Clone("variance")
    varVector_c = unfold_c.EunfoldV(ROOT.RooUnfolding.kErrors)
    countsVector_c = unfold_c.Vunfold()
    for i in range(varVector_c.GetNrows()):
        if countsVector_c[i] > 0:
            varhist_c.SetBinContent(i+1,varVector_c[i]*varVector_c[i]/countsVector_c[i])
            varhist_c.SetBinError(i+1,0)
        else:
            varhist_c.SetBinContent(i+1,varVector_c[i]*varVector_c[i])    
            varhist_c.SetBinError(i+1,0)

    # Plot all the diagnostic hists
    pad2.cd()
    #pad2.SetLogy()
    y1 = covhist_c.GetYaxis()
    y1.SetRangeUser(-.05,.75)
    y1.SetLabelSize(.15)
    y1.SetTitle("Coverage")
    y1.SetTitleSize(.2)
    y1.SetTitleOffset(.18)
    y1.SetNdivisions(6)
    x1 = covhist_c.GetXaxis()
    if hist_def['vbinning']:
        x1.SetRangeUser(hist_def['truth_histogram'][0],hist_def['truth_histogram'][-1])
    else:
        x1.SetRangeUser(hist_def['truth_histogram'][1],hist_def['truth_histogram'][-1])
    x1.SetLabelSize(.1)

    covhist_c.SetStats(0)
    covhist_c.SetTitle("")
    covhist_c.SetLineWidth(3)
    covhist_c.SetLineColor(29)
    covhist_c.Draw("E")

    line1 = ROOT.TLine(0,0.6727,20,0.6727)
    line1.SetLineColor(ROOT.kRed)
    line1.SetLineWidth(2)
    line1.Draw("same")

    pad3.cd()
    pad3.SetLogy()
    y3 = varhist_c.GetYaxis()
    maximum = varhist_c.GetMaximum()
    y3.SetRangeUser(0.05,maximum+.1*maximum)
    y3.SetLabelSize(.15)
    y3.SetTitle("Var_{rel}")
    y3.SetTitleSize(.2)
    y3.SetTitleOffset(.18)
    y3.SetNdivisions(6)
    x3 = varhist_c.GetXaxis()
    x3.SetRangeUser(100,200)
    x3.SetLabelSize(.1)

    varhist_c.SetStats(0)
    varhist_c.SetTitle("")
    varhist_c.SetLineWidth(3)
    varhist_c.SetLineColor(29)
    varhist_c.Draw("E")
            
    pad4.cd()
    y2 = biashist_cRMS.GetYaxis()
    y2.SetRangeUser(-1.,1)
    y2.SetLabelSize(.15)
    y2.SetTitle("Bias")
    y2.SetTitleSize(.2)
    y2.SetTitleOffset(.2)
    y2.SetNdivisions(6)
    x2 = biashist_cRMS.GetXaxis()
    if hist_def['vbinning']:
        x2.SetRangeUser(hist_def['truth_histogram'][0],hist_def['truth_histogram'][-1])
    else:
        x2.SetRangeUser(hist_def['truth_histogram'][1],hist_def['truth_histogram'][-1])
    x2.SetLabelSize(.15)
    x2.SetTitle("x")
    x2.SetTitleSize(.2)
    x2.SetTitleOffset(.5)

    biashist_cRMS.SetStats(0)
    biashist_cRMS.SetTitle("")
    biashist_c.SetLineWidth(3)
    biashist_c.SetLineColor(29)
    #biashist_c.SetMarkerStyle(20)
    biashist_cRMS.SetFillColor(29)
    biashist_cRMS.SetFillStyle(3002)

    biashist_cRMS.Draw("E2")
    biashist_c.Draw("Esame")
    
    c.SaveAs("{}/unfolded_{}.{}".format(out, name, fmt))
        
def response_plotter(response, name, hist_def, out, fmt):
    print("definitely", hist_def)

    ROOT.gROOT.SetStyle("ATLAS")
    c1 = ROOT.TCanvas("c", "", 600, 600)
    pad = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
    pad.SetTickx(False)
    pad.SetTicky(False)
    if hist_def["logx"]:
        pad.SetLogx()
    if hist_def["logx"]:
        pad.SetLogy()
    pad.Draw()
    pad.cd()
    R = response.HresponseNoOverflow()
    R.SetStats(0)
    R.Draw("Colz")
    xaxis = R.GetXaxis()
    xaxis.SetLabelSize(0.04)
    xaxis.SetTitleSize(0.045)
    xaxis.SetTitleOffset(.8)
    xaxis.SetTitle(hist_def["title"])

    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.045)
    text.DrawLatex(0.21, 0.86, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.21 + 0.16, 0.86, "Preliminary")
    text.SetTextSize(0.04)
    text.DrawLatex(0.21, 0.80, "#sqrt{{s}} = 13 TeV, {:.1f} fb^{{-1}}".format(0.1))

    #c.Draw()
    c1.SaveAs("{}/{}_response.{}".format(out,name,fmt))

   # # Purity
   # ROOT.gROOT.SetStyle("ATLAS")
   # c2 = ROOT.TCanvas("c2", "", 600, 600)
   # pad2 = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
   # pad2.SetTickx(False)
   # pad2.SetTicky(False)
   # if hist_def["logy"]:
   #     pad2.SetLogy()
   # if hist_def["logx"]:
   #     pad2.SetLogy()
   # pad2.Draw()
   # pad2.cd()
   # if hist_def["vbinning"]:
   #     n = len(hist_def['truth_histogram']) -1
   #     hPurity = ROOT.TH1D("Purity","purity",n,arr.array('f',hist_def['truth_histogram']))
   # else:
   #     n, n_min, n_max = hist_def['truth_histogram']
   #     hPurity = ROOT.TH1D("Purity","purity", n, n_min, n_max)
   # Vpur = response.Vpurity()
   # for i in range(hPurity.GetNbinsX()):
   #     hPurity.SetBinContent(i+1,Vpur(i))
   # hPurity.SetStats(0)
   # hPurity.SetLineWidth(1)
   # hPurity.SetLineColor(1)
   # hPurity.SetFillColor(38)
   # hPurity.Draw("HIST")

   # xaxis = hPurity.GetXaxis()
   # xaxis.SetLabelSize(0.04)
   # xaxis.SetTitleSize(0.045)
   # xaxis.SetTitleOffset(.8)
   # xaxis.SetTitle(hist_def["title"])

   # legend1 = ROOT.TLegend(0.6, 0.85, 0.92, 0.92)
   # legend1.SetTextFont(42)
   # legend1.SetFillStyle(0)
   # legend1.SetBorderSize(0)
   # legend1.SetTextSize(0.02)
   # legend1.SetTextAlign(32)
   # legend1.AddEntry(hPurity, "{} Purity".format(name), "f")
   # legend1.Draw("SAME")
   # 
   # # Add ATLAS label
   # text = ROOT.TLatex()
   # text.SetNDC()
   # text.SetTextFont(72)
   # text.SetTextSize(0.045)
   # text.DrawLatex(0.21, 0.86, "ATLAS")
   # text.SetTextFont(42)
   # text.DrawLatex(0.21 + 0.16, 0.86, "Preliminary")
   # text.SetTextSize(0.04)
   # text.DrawLatex(0.21, 0.80, "#sqrt{{s}} = 13 TeV, {:.1f} fb^{{-1}}".format(0.1))

   # c2.SaveAs("{}/{}_purity.{}".format(out,name,fmt))
    
    # Efficiency
   # c3 = ROOT.TCanvas("c3", "", 600, 600)
   # pad3 = ROOT.TPad("upper_pad", "", 0, 0, 1, 1)
   # pad3.SetTickx(False)
   # pad3.SetTicky(False)
   # if hist_def["logy"]:
   #     pad3.SetLogy()
   # if hist_def["logx"]:
   #     pad3.SetLogy()
   # pad3.Draw()
   # pad3.cd()
   # if hist_def["vbinning"]:
   #     n = len(hist_def['truth_histogram']) -1        
   #     hEff = ROOT.TH1D("efficiency","efficiency", n, arr.array('f',hist_def['truth_histogram']))
   # else:
   #     n, n_min, n_max = hist_def['truth_histogram']
   #     hEff = ROOT.TH1D("efficiency","efficiency", n, n_min, n_max)
   # Veff = response.Vefficiency()
   # for i in range(hEff.GetNbinsX()):
   #     hEff.SetBinContent(i+1,Veff(i))
   # hEff.SetStats(0)
   # hEff.SetLineWidth(1)
   # hEff.SetLineColor(1)
   # hEff.SetFillColor(38)
   # hEff.Draw("HIST")

   # xaxis = hEff.GetXaxis()
   # xaxis.SetLabelSize(0.04)
   # xaxis.SetTitleSize(0.045)
   # xaxis.SetTitleOffset(.8)
   # xaxis.SetTitle(hist_def["title"])

   # legend2 = ROOT.TLegend(0.6, 0.85, 0.92, 0.92)
   # legend2.SetTextFont(42)
   # legend2.SetFillStyle(0)
   # legend2.SetBorderSize(0)
   # legend2.SetTextSize(0.02)
   # legend2.SetTextAlign(32)
   # legend2.AddEntry(hEff, "{} efficiency".format(name), "f")
   # legend2.Draw("SAME")
   # 
   # # Add ATLAS label
   # text = ROOT.TLatex()
   # text.SetNDC()
   # text.SetTextFont(72)
   # text.SetTextSize(0.045)
   # text.DrawLatex(0.21, 0.86, "ATLAS")
   # text.SetTextFont(42)
   # text.DrawLatex(0.21 + 0.16, 0.86, "Preliminary")
   # text.SetTextSize(0.04)
   # text.DrawLatex(0.21, 0.80, "#sqrt{{s}} = 13 TeV, {:.1f} fb^{{-1}}".format(0.1))

   # c3.SaveAs("{}/{}_efficiency.{}".format(out,name,fmt))
