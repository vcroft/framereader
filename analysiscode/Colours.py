def get_colours(sample_name):
    if "_B" in sample_name:
        return 46
    elif "_NB" in sample_name:
        return 49
    elif "_C" in sample_name:
        return 9
    elif "_NC" in sample_name:
        return 38
    elif "_M" in sample_name:
        return 29
    elif "_L" in sample_name:
        return 42
    elif "data" in sample_name:
        return 1
    else:
        return 29
